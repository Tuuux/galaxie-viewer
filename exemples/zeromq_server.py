#
#   Hello World server in Python
#   Binds REP socket to tcp://*:5555
#   Expects b"Hello" from client, replies with b"World"
#

import time
import zmq
import json
import glxviewer

context = zmq.Context()
socket = context.socket(zmq.REP)
socket.bind("tcp://*:5555")
viewer = glxviewer.Viewer()

viewer.write(
    column_1="Start the hello world server…",
    status_text='INFO',
    status_text_color='YELLOW',
    status_symbol=None,
)
try:
    while True:
        #  Wait for next request from client
        received_message = json.loads(socket.recv())
        viewer.write(
            column_1=json.dumps(received_message),
            status_text='RCV',
            status_text_color='RED',
            status_symbol='<',
        )

        #  Do some 'work'
        time.sleep(0.5)

        #  Send reply back to client
        returned_message = {
            'code': 200,
            'id': received_message['id'],
            'data': received_message['data'],
        }
        viewer.write(
            column_1="%s" % returned_message,
            status_text='SND',
            status_text_color='GREEN',
            status_symbol='>',
        )
        socket.send(json.dumps(returned_message).encode())

except KeyboardInterrupt:
    viewer.flush_a_new_line()
    exit()