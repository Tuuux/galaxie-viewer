#
#   Hello World client in Python
#   Connects REQ socket to tcp://localhost:5555
#   Sends "Hello" to server, expects "World" back
#

import zmq
import glxviewer
import uuid
import json

context = zmq.Context()
viewer = glxviewer.Viewer()

#  Socket to talk to server
viewer.write(
    column_1="Connecting to hello world server…",
    status_text='INFO',
    status_text_color='YELLOW',
    status_symbol=None,
)
socket = context.socket(zmq.REQ)
socket.connect("tcp://localhost:5555")

#  Do 10 requests, waiting each time for a response
for request in range(1, 11, 1):
    message = {
        "id": request,
        "data": {
            "uuid": str(uuid.uuid4())
        }
    }
    viewer.write(
        column_1="{0}".format(message),
        status_text='SND',
        status_text_color='GREEN',
        status_symbol='>',
    )

    socket.send(json.dumps(message).encode())

    #  Get the reply.
    received_message = json.loads(socket.recv())

    viewer.write(
        column_1="{0}".format(received_message),
        status_text='RCV',
        status_text_color='RED',
        status_symbol='<',
    )
