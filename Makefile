.PHONY: help install-python header venv prepare docs tests clean
SHELL=/bin/sh

#!make
include .env
export $(shell sed 's/=.*//' .env)

help:
	@echo  'make clean      - Remove every created directory and restart from scratch'
	@echo  'make docs       - Install Sphinx and requirements, then build documentations'
	@echo  'make prepare    - Easy way for make everything'
	@echo  'make tests      - Easy way for make tests over a CI'
	@echo  ''
	@echo  'sudo make install-python   - Easy way to install python3 pip and venv'

header:
	@echo "**************************** GALAXIE VIEWER MAKEFILE ***************************"
	@echo "HOSTNAME	`uname -n`"
	@echo "KERNEL RELEASE `uname -r`"
	@echo "KERNEL VERSION `uname -v`"
	@echo "PROCESSOR	`uname -m`"
	@echo "********************************************************************************"


install-python:
	@ echo ""
	@ echo "**************************** INSTALL PYTHON PACKAGES ***************************"
	apt-get update && apt install -y python3 python3-pip python3-venv

venv: header
	@ if test -d $(DEV_VENV_DIRECTORY);\
	then echo "[  OK  ] VIRTUAL ENVIRONMENT";\
	else echo "[CREATE] VIRTUAL ENVIRONMENT" &&\
		cd $(DEV_DIRECTORY) && python3 -m venv $(DEV_VENV_NAME);\
	fi

	@${DEV_VENV_ACTIVATE} && pip3 install -U pip --no-cache-dir --quiet &&\
	echo "[  OK  ] PIP3" || \
	echo "[FAILED] PIP3"

	@${DEV_VENV_ACTIVATE} && pip3 install -U wheel --no-cache-dir --quiet &&\
	echo "[  OK  ] WHEEL" || \
	echo "[FAILED] WHEEL"

	@${DEV_VENV_ACTIVATE} && pip3 install -U setuptools --no-cache-dir --quiet &&\
	echo "[  OK  ] SETUPTOOLS" || \
	echo "[FAILED] SETUPTOOLS"

	@${DEV_VENV_ACTIVATE} && pip3 install -U green --no-cache-dir --quiet &&\
	echo "[  OK  ] GREEN" || \
	echo "[FAILED] GREEN"

prepare: venv
	@ echo ""
	@ echo "*********************************** PREPARE ************************************"
	@ ${DEV_VENV_ACTIVATE} && pip install -e .

docs: prepare
	@ echo ""
	@ echo "***************************** BUILD DOCUMENTATIONS *****************************"
	@${DEV_VENV_ACTIVATE} && pip3 install -U --no-cache-dir --quiet -r docs/requirements.txt &&\
	echo "[  OK  ] REQUIREMENTS" || \
	echo "[FAILED] REQUIREMENTS"
	@ ${DEV_VENV_ACTIVATE} &&\
	pip3 install --upgrade pip setuptools wheel --no-cache-dir --quiet &&\
  	pip3 install -r docs/requirements.txt --no-cache-dir --quiet &&\
	cd $(DEV_DIRECTORY)/docs &&\
	sphinx-apidoc -e -f -o source/ ../glxviewer/ &&\
	make html


tests: prepare
	@ echo ""
	@ echo "********************************** START TESTS *********************************"
	@ ${DEV_VENV_ACTIVATE} && python setup.py tests

clean: header
	@ echo ""
	@ echo "*********************************** CLEAN UP ***********************************"
	rm -rf ./venv
	rm -rf ~/.cache/pip
	rm -rf ./.eggs
	rm -rf ./*.egg-info
	rm -rf .coverage
	rm -rf docs/build