==============
GALAXIE VIEWER
==============
CHANGELOG
---------
**0.5.2 - 20211115**
  * Update example in documentation
  * Sphinx fixe warning
  * Migrate to single file module
  * Better CI/CD version system
**0.5.1 - 20211113**
  * rename ``flush_infos`` to ``write``
  * better **setuptools** management
**0.5.0 - 20211113**
  * Migrate to ``py-term`` for color
  * Migrate to ``py-term`` for cursor position
  * Code refactoring
**0.4.6 - 20211113**
  * better ``Makefile``
**0.4.5 - 20210205**
  * add viewer variable in ``__all__``
  * many change about refactoring
  * restore input capability
**0.4.4 - 20210205**
  * rename the module GLXViewer to glxviewer
**0.4.3 - 20210125**
  * Fixe CI troubles
  * CI use only ``Makefile``
  * Change documentation theme
  * Remove Markdown usage
  * Version is centralize inside ``__init__.py`` file
**0.4.2 - 20210125**
  * Make tests with the Makefile
  * Add .env file
**0.4.1 - 20210125**
  * Refactoring
  * Remove python2/python3 mess about str type
**0.4 - 20210125**
  * Better port to Python 3
  * Black reformat
