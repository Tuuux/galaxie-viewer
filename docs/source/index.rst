.. Galaxie Viewer documentation master file, created by
   sphinx-quickstart on Thu Oct 17 21:44:02 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. title:: index

==============================
Galaxie Viewer's documentation
==============================
.. figure:: images/logo_galaxie.png
   :align:  center

Description
-----------
Provide a Text Based line viewer, it use a template. It existe many template for high level language, but nothing for text one.

Our mission is to provide useful display template for terminal. Actually every Galaxie tool use it; where print() is not use any more...

Links
-----
 * GitLab: https://gitlab.com/Tuuux/galaxie-viewer/
 * Read the Doc: https://glxviewer.readthedocs.io/
 * PyPI: https://pypi.org/project/galaxie-viewer/
 * PyPI Test: https://test.pypi.org/project/galaxie-viewer/


Screenshots
-----------
v 0.5

.. figure::  images/screen_01.png
   :align:   center

Contents
--------
.. toctree::
   :maxdepth: 2

   modules

Installation via pip
--------------------
Pypi

.. code:: bash

  pip install galaxie-viewer

Pypi Test

.. code:: bash

  pip install -i https://test.pypi.org/simple/ galaxie-viewer


Exemple
-------
.. code:: python

   import sys
   import os
   import time

   from glxviewer import viewer


   def main():
       start_time = time.time()
       viewer.write(
           column_1=__file__,
           column_2='Yes that is possible',
           status_text_color='MAGENTA',
       )
       viewer.write(
           column_1=__file__,
           column_2='it have no difficulty to make it',
           column_3='what ?'
       )
       viewer.write(
           column_1='Use you keyboard with Ctrl + c for stop the demo',
           status_text='INFO',
           status_text_color='GREEN',
           status_symbol='!',
       )
       while True:

           viewer.write(
               column_1=__file__,
               column_2=str(time.time() - start_time),
               status_text='REC',
               status_text_color='RED',
               status_symbol='<',
               prompt=-1
           )


   if __name__ == '__main__':
       try:
           main()
       except KeyboardInterrupt:
           viewer.flush_a_new_line()
           sys.exit()

License
-------

.. toctree::
   :maxdepth: 0

   LICENSE

See the LICENCE_

.. _LICENCE: https://gitlab.com/Tuuux/galaxie-viewer/blob/master/LICENSE.rst

All contributions to the project source code ("patches") SHALL use the same license as the project.


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
