#!/usr/bin/env python
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: the Galaxie Viewer Team, all rights reserved

import sys
import unittest
from io import StringIO
from unittest.mock import patch

import shutil
import glxviewer
from glxviewer import Viewer
from glxviewer import viewer


class TestView(unittest.TestCase):
    # Test
    def test_singleton(self):
        viewer1 = Viewer()
        viewer2 = Viewer()

        self.assertEqual(viewer1, viewer2)

    def test_Viewer_property_with_date(self):
        # viewer = Viewer()
        self.assertTrue(viewer.with_date)

        viewer.with_date = False
        self.assertFalse(viewer.with_date)

        self.assertRaises(TypeError, setattr, viewer, 'with_date', 42)

    def test_Viewer_property_status_text(self):
        # viewer = Viewer()
        self.assertEqual(viewer.status_text, "DEBUG")

        viewer.status_text = "42"
        self.assertEqual(viewer.status_text, "42")

        self.assertRaises(TypeError, setattr, viewer, 'status_text', 42)

    def test_Viewer_property_status_text_color(self):
        # viewer = Viewer()
        self.assertEqual(viewer.status_text_color, "WHITE")

        viewer.status_text_color = "CYAN"
        self.assertEqual(viewer.status_text_color, "CYAN")

        self.assertRaises(TypeError, setattr, viewer, 'status_text_color', 42)
        self.assertRaises(ValueError, setattr, viewer, 'status_text_color', "42")

    def test_Viewer_property_status_symbol(self):
        # viewer = Viewer()
        self.assertEqual(viewer.status_symbol, " ")

        viewer.status_symbol = "<"
        self.assertEqual(viewer.status_symbol, "<")

        self.assertRaises(TypeError, setattr, viewer, 'status_symbol', 42)

    def test_Viewer_property_text_column_1(self):
        # viewer = Viewer()
        self.assertEqual(viewer.text_column_1, "")

        viewer.text_column_1 = "42"
        self.assertEqual(viewer.text_column_1, "42")

        self.assertRaises(TypeError, setattr, viewer, 'text_column_1', 42)

    def test_Viewer_property_text_column_2(self):
        # viewer = Viewer()
        self.assertEqual(viewer.text_column_2, "")

        viewer.text_column_2 = "42"
        self.assertEqual(viewer.text_column_2, "42")

        self.assertRaises(TypeError, setattr, viewer, 'text_column_2', 42)

    def test_Viewer_property_text_column_3(self):
        # viewer = Viewer()
        self.assertEqual(viewer.text_column_3, "")

        viewer.text_column_3 = "42"
        self.assertEqual(viewer.text_column_3, "42")

        self.assertRaises(TypeError, setattr, viewer, 'text_column_3', 42)

    def test_flush_infos(self):
        self.assertRaises(
            ValueError,
            viewer.write,
            with_date=True,
            status_text="DEBUG",
            status_text_color="42",
            status_symbol=None,
            column_1=None,
            column_2=None,
            column_3=None,
            prompt=None
        )
        color_list = ["BLACK", "RED", "GREEN", "YELLOW", "BLUE", "MAGENTA", "CYAN", "WHITE"]
        for color in color_list:
            viewer.write(
                with_date=True,
                status_text="TEST",
                status_text_color=color,
                column_1="test with {0} color".format(color),
            )

        for color in color_list:
            viewer.write(
                with_date=False,
                status_text="TEST",
                status_text_color=color,
                column_1="test with {0} color".format(color),
            )

        for color in color_list:
            viewer.write(
                with_date=True,
                status_text="TEST",
                status_text_color=color,
                column_1="test with {0} color".format(color),
                prompt=-1,
                status_symbol='<'
            )
            viewer.write(
                with_date=False,
                status_text="TEST",
                status_text_color=color,
                column_1="test with {0} color".format(color),
                prompt=-1,
                status_symbol='<'
            )

        for color in color_list:
            viewer.write(
                with_date=True,
                status_text="TEST",
                status_text_color=color,
                column_1="test with {0} color".format(color),
                prompt=None,
                status_symbol='>'
            )

        for color in color_list:
            viewer.write(
                with_date=True,
                status_text="TEST",
                status_text_color=color,
                column_1="test with {0} color".format(color),
                prompt=1
            )
        sys.stdout.write("\n")
        sys.stdout.flush()

    def test_flush_new_line(self):
        # viewer = Viewer()
        with patch("sys.stdout", new=StringIO()) as fake_out:
            viewer.flush_a_new_line()
            self.assertEqual(fake_out.getvalue(), "\n")

    def test_utils_center_text(self):

        self.assertRaises(TypeError, glxviewer.center_text, text='', max_width='')
        self.assertRaises(TypeError, glxviewer.center_text, text=None, max_width=None)

        self.assertEqual("  *  ", glxviewer.center_text(text="*", max_width=5))
        self.assertEqual("  TX ", glxviewer.center_text(text="TX", max_width=5))
        self.assertEqual("  RX ", glxviewer.center_text(text="RX", max_width=5))
        self.assertEqual(" DLA ", glxviewer.center_text(text="DLA", max_width=5))
        self.assertEqual(" INIT ", glxviewer.center_text(text="INIT", max_width=5))
        self.assertEqual(" DEBUG ", glxviewer.center_text(text="DEBUG", max_width=5))

        width = shutil.get_terminal_size().columns
        if width == 0 or width is None:
            width = 80
        self.assertEqual(
            width, len(glxviewer.center_text(text="DEBUG", max_width=width))
        )

    def test_utils_text(self):
        """Test utils.resize_text()"""
        text = "123456789"
        width = 10
        self.assertEqual(text, glxviewer.resize_text(text, width, "~"))
        width = 9
        self.assertEqual(text, glxviewer.resize_text(text, width, "~"))
        width = 8
        self.assertEqual("123~789", glxviewer.resize_text(text, width, "~"))
        width = 7
        self.assertEqual("123~789", glxviewer.resize_text(text, width, "~"))
        width = 6
        self.assertEqual("12~89", glxviewer.resize_text(text, width, "~"))
        width = 5
        self.assertEqual("12~89", glxviewer.resize_text(text, width, "~"))
        width = 4
        self.assertEqual("1~9", glxviewer.resize_text(text, width, "~"))
        width = 3
        self.assertEqual("1~9", glxviewer.resize_text(text, width, "~"))
        width = 2
        self.assertEqual("19", glxviewer.resize_text(text, width, "~"))
        width = 1
        self.assertEqual("1", glxviewer.resize_text(text, width, "~"))
        width = 0
        self.assertEqual("", glxviewer.resize_text(text, width, "~"))
        width = -1
        self.assertEqual("", glxviewer.resize_text(text, width, "~"))

        # Test Error
        self.assertRaises(
            TypeError,
            glxviewer.resize_text,
            text=text,
            max_width=width,
            separator=int(42),
        )
        self.assertRaises(
            TypeError, glxviewer.resize_text, text=text, max_width="coucou", separator="~"
        )
        self.assertRaises(
            TypeError, glxviewer.resize_text, text=int(42), max_width=width, separator="~"
        )

    def test_bracket_test(self):

        self.assertRaises(TypeError, glxviewer.bracket_text, text=None, symbol_inner="[", symbol_outer="]")
        self.assertRaises(TypeError, glxviewer.bracket_text, text='Hello', symbol_inner=42, symbol_outer="]")
        self.assertRaises(TypeError, glxviewer.bracket_text, text='Hello', symbol_inner='42', symbol_outer=42)
        self.assertEqual(glxviewer.bracket_text(text="Hello"), "[Hello]")


if __name__ == "__main__":
    unittest.main()
